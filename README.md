CSC 541: Assignment #5 - External Chained Hashing
==============================
The goal of this assignment is to study an implementation of file-based 
chained hashing. Your program will work simultaneously with two files. The 
first file, used to hold a chained hash representation of primary keys, acts 
as a primary index. The second file, maintained in order of insertion, stores 
the actual records that make up your database.

Development environment
------------------------------ 
Visual Studio 2012 Ultimate on Windows 7 Home Premium 64 bit

Testing
------------------------------ 
 * Executables Tested with Visual Studio 2010 VCL image 
 * Test inputs present in source/Test_Files
 * Generated output present in source/Output
