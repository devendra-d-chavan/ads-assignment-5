#include "filereader.h"
#include "record.h"
#include "node.h"

#define TBL_SIZE 1001

class ChainedHash
{
private:
	filereader *file_handler;

	Node Deserialize(long offset);
	void Serialize(Node node, long offset);

	long GetHomeAddress(int key);
	void InsertEnd(Record record);

public:
	ChainedHash(string index_filepath);
	~ChainedHash();
	bool Add(int key, long offset);
	long Find(int key);
	bool Delete(int key);
	void End();
	void Print();
};