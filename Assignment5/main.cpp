#include <string>
#include <iostream>
#include <iomanip>  
#include <windows.h>
#include <ctime>

#include "record_manager.h"

#define MAX_USER_INPUT_LEN 200
#define MAX_USER_INPUT_TOKENS 10

using std::cin;
using std::cout;
using std::endl;

int bst(int argc, char *argv[]);
int btree(int argc, char *argv[]);

int main( int argc, char *argv[] )
{
	if(argc != 3)
	{
		cout << "Program usage : assn_5 <index filename> <record filename>. For example, assn_5 db.idx db.dat"
			<< endl;

		return 1;
	}

	string index_name = (char*)argv[1];	
	string datafile_name = (char*)argv[2];	

	string buf[MAX_USER_INPUT_TOKENS];
	bool end;
	char user_input[MAX_USER_INPUT_LEN];

	RecordManager *record_manager = new RecordManager(index_name, datafile_name);

	do
	{
		end = false;
		cin.getline(user_input, MAX_USER_INPUT_LEN);
		string input = user_input;

		input.token(buf, MAX_USER_INPUT_TOKENS);

		string command = buf[0];
		string argument;
		if(command == "add")
		{
			argument = input.substr(command.len() + 1, input.len() - 1);
		}
		else if(command == "find" || command == "delete")
		{
			argument = buf[1];
		}

		if(command == "add")
		{
			Record record(argument);
			if(!record_manager->Add(record))
			{
				cout << "Record " << record.num << " is a duplicate." << endl;
			}
		}
		else if(command == "find")
		{
			if(!record_manager->Find(argument))
			{
				cout << "Record " << argument << " does not exist." << endl;
			}
		}
		else if(command == "delete")
		{
			if(!record_manager->Delete(argument))
			{
				cout << "Record " << argument << " does not exist." << endl;
			}
		}
		else if(command == "end")
		{
			record_manager->End();
			end = true;
		}
		else if(command == "print")
		{
			record_manager->Print();
		}
		else
		{
			cout << "Invalid input";
		}
	}while(!end);

	return 0;
}