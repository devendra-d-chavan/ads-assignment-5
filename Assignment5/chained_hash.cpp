#include "chained_hash.h"

using std::cout;
using std::endl;

ChainedHash::ChainedHash(string index_filepath)
{
	this->file_handler = new filereader();

	// Clear the previous contents
	this->file_handler->open(index_filepath, 'w', 1, 1);
	this->file_handler->close();

	// Initialize the index file
	this->file_handler->open(index_filepath, 'x', 1, 1);
	
	// If file empty, append 1001 nodes with a key=-1
	this->file_handler->seek(0, seek_mode::END);

	if (this->file_handler->offset() == 0) 
	{
		Node node;
		node.k = -1;
		for(int i = 0; i < TBL_SIZE	; i++) 
		{
			this->file_handler->write_raw((char *)&node, sizeof(Node));
		}
	}
}

ChainedHash::~ChainedHash()
{
	this->End();
}

void ChainedHash::End()
{
	if(this->file_handler != NULL)
	{
		this->file_handler->close();
		this->file_handler = NULL;
	}
}

long ChainedHash::GetHomeAddress(int key)
{
	return sizeof(Node) * (key%TBL_SIZE);
}

void ChainedHash::Serialize(Node node, long offset)
{
	this->file_handler->seek(offset, seek_mode::BEGIN);
	this->file_handler->write_raw((char*)&node, sizeof(node));
}

Node ChainedHash::Deserialize(long offset)
{
	// Check if the file is empty
	this->file_handler->seek(offset, seek_mode::END);
	if(this->file_handler->offset() == 0)
	{
		throw std::runtime_error("Empty file");
	}

	Node node;
	this->file_handler->seek(offset, seek_mode::BEGIN);
	this->file_handler->read_raw((char*)&node, sizeof(Node));

	return node;
}	

bool ChainedHash::Add(int key, long record_offset)
{
	long home_address = GetHomeAddress(key);

	long current_node_offset = home_address;

	Node node = Deserialize(home_address);
	bool primary_location_free = node.next == -1 && node.k == -1;

	while(node.next != -1 && node.k != key)
	{
		current_node_offset = node.next;
		node = Deserialize(node.next);
	}

	if(node.k == key)
	{
		// Duplicate record
		return false;
	}

	// If either the primary location has been filled
	if(primary_location_free)
	{
		node.k = key;
		node.rec = record_offset;
		this->Serialize(node, current_node_offset);
	}
	else
	{
		// Node not found and reached end of the hash chain
		// Insert at the end of the file
		this->file_handler->seek(0, seek_mode::END);
		long new_offset = this->file_handler->offset();
		node.next = new_offset;
		this->Serialize(node, current_node_offset);

		Node new_node(key, record_offset);
		this->Serialize(new_node, new_offset);
	}	

	return true;
}

long ChainedHash::Find(int key)
{
	long home_address = GetHomeAddress(key);

	Node node = Deserialize(home_address);
	while(node.next != -1 && node.k != key)
	{
		home_address = node.next;
		node = Deserialize(node.next);
	}

	if(node.k == key)
	{
		return node.rec;
	}

	return -1;
}

bool ChainedHash::Delete(int key)
{
	long home_address = GetHomeAddress(key);

	long current_node_offset = home_address;

	Node node = Deserialize(home_address);
	Node prev_node;
	long prev_offset;

	while(node.next != -1 && node.k != key)
	{
		prev_node = node;
		prev_offset = current_node_offset;

		current_node_offset = node.next;
		node = Deserialize(node.next);
	}

	if(node.k != key)
	{
		return false;
	}
	
	if(prev_node.next != -1)
	{
		// Change the links of the previous node
		prev_node.next = node.next;
		this->Serialize(prev_node, prev_offset);
	}

	// Mark the node as deleted
	node.k = -1;
	node.rec = -1;
	//node.next = -1;

	this->Serialize(node, current_node_offset);

	return true;
}

void ChainedHash::Print()
{
	this->file_handler->seek(0, seek_mode::BEGIN);
	
	for(int i = 0; i < TBL_SIZE	; i++) 
	{
		Node node = this->Deserialize(i*sizeof(Node));

		cout << i << ": ";
		if(node.k != -1)
		{
			cout << node.k << "/" << node.rec << " ";
		}

		while(node.next != -1)
		{
			node = Deserialize(node.next);
			if(node.k != -1)
			{
				cout << node.k << "/" << node.rec << " ";
			}
		}

		cout << endl;
	}
}
