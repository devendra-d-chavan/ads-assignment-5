#include "filereader.h"
#include "chained_hash.h"

class RecordManager
{
private:
	filereader *file_handler;
	ChainedHash *chained_hash;

	Record Deserialize(long offset);
	void Serialize(Record record, long offset);
public:
	RecordManager(string data_filepath, string index_filepath);
	~RecordManager();
	bool Add(Record record);
	bool Find(int key);
	bool Delete(int key);
	void End();
	void Print();
};