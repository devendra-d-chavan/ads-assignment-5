typedef struct ch_node {	// Chained hash table node
	int k;		// Key of record node represents
	long rec;	// Offset of corresponding record in database file
	long next;	// Offset of next node in this chain (-1 for none)

	ch_node()
	{
		k = -1;
		rec = -1;
		next = -1;
	}

	ch_node(int key, long record_offset) : k(key), rec(record_offset)
	{
		next = -1;
	}
} Node;