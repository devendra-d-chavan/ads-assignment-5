#include "str.h"

typedef struct f_rec {	// Financial record structure
	int num;	// Transaction number
	float amt;	// Amount of the transaction
	char type;	// Transaction type (D=debit, C=credit)
	int acct;	// Account used for transaction
	int hour;	// Hour of transaction (0-23)
	int min;	// Minute of transaction (0-59)
	int day;	// Day of transaction (0-364)
	int year;	// Year of transaction (0000-9999)	

	f_rec()
	{
		// Do nothing
	}

	f_rec(string str)
	{
		string buf[8]; 
		str.token(buf, 8);

		this->num = atoi(buf[0]	);
		this->amt = float(atof(buf[1]));
		this->type = buf[2][0];
		this->acct = atoi(buf[3]);
		this->hour = atoi(buf[4]);
		this->min = atoi(buf[5]);
		this->day = atoi(buf[6]);
		this->year = atoi(buf[7]);
	}	
} Record;