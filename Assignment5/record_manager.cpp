#include <iomanip>  
#include "record_manager.h"

using std::cout;
using std::endl;

RecordManager::RecordManager(string data_filepath, string index_filepath)
{
	this->chained_hash = new ChainedHash(index_filepath);
	this->file_handler = new filereader();
	
	// Clear the previous contents
	this->file_handler->open(data_filepath, 'w', 1, 1);
	this->file_handler->close();

	this->file_handler->open(data_filepath, 'x', 1, 1);
}

RecordManager::~RecordManager()
{
	this->End();
}

void RecordManager::End()
{
	if(this->file_handler != NULL)
	{
		this->file_handler->close();
		this->file_handler = NULL;
	}
}

void RecordManager::Serialize(Record record, long offset)
{
	// TODO: Seek/offset is not needed as the file is always written sequentially
	this->file_handler->seek(offset, seek_mode::BEGIN);
	this->file_handler->write_raw((char*)&record, sizeof(record));
}

Record RecordManager::Deserialize(long offset)
{
	// Check if the file is empty
	this->file_handler->seek(offset, seek_mode::END);
	if(this->file_handler->offset() == 0)
	{
		throw std::runtime_error("Empty file");
	}

	Record record;
	this->file_handler->seek(offset, seek_mode::BEGIN);
	this->file_handler->read_raw((char*)&record, sizeof(record));

	return record;
}

bool RecordManager::Add(Record record)
{
	// Get the offset
	// TODO: Seek/offset is not needed as the file is always written sequentially
	this->file_handler->seek(0, seek_mode::END);
	long new_offset = this->file_handler->offset();
	
	// Try to add to the index file
	if(!this->chained_hash->Add(record.num, new_offset))
	{
		return false;
	}

	// Able to add to the index file (no duplicates), now add to the data file
	this->Serialize(record, new_offset);
	return true;
}

bool RecordManager::Find(int key)
{
	// Check whether the key exists in the index
	long record_offset = this->chained_hash->Find(key);
	if(record_offset == -1)
	{
		return false;
	}

	Record record = this->Deserialize(record_offset);
	
	cout.unsetf(std::ios::floatfield);
	cout << record.num << " ";
	printf("%g ", record.amt);
	cout << record.type << " " 
		 << record.acct << " "
		 << record.hour << " "
		 << record.min << " "
		 << record.day << " "
		 << record.year << endl;

	return true;
}

bool RecordManager::Delete(int key)
{
	// Remove from the index, data file is not required to be updated
	return this->chained_hash->Delete(key);
}

void RecordManager::Print()
{
	this->chained_hash->Print();
}